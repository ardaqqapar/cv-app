export default {
    testEnvironment: 'jest-environment-jsdom',
    transform: {
        "^.+\\.jsx?$": "babel-jest",
        "^.+\\.scss$": 'jest-scss-transform',
      },
    collectCoverageFrom: ['src/**/*.{js,jsx}'],
    coverageThreshold: {
      global: {
        branches: 35,
        functions: 35,
        lines: 35,
        statements: 35,
      },
    },
  };