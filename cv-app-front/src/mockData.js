import guy from './assets/guy.png'
import card1 from './assets/Portfolio/card_1.png'
import card2 from './assets/Portfolio/card_2.png'

export const experience = 
    [
        {
            id: 1,
            date: '2013',
            company: 'Google',
            job: 'Front-end developer / php programmer',
            description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor'
        },
        {
            id: 2,
            date: '2012', 
            company: 'Twitter',
            job: 'Web developer',
            description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor'
        }
    ]

export const feedbacks = 
    [ 
        {
            id: 1,
            feedback: ' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor', 
            reporter: { 
                photoUrl: guy, 
                name: 'John Doe', 
                citeUrl: 'https://www.citeexample.com' 
            } 
        }, 
        {
            id: 2,
            feedback: ' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor', 
            reporter: { 
                photoUrl: guy, 
                name: 'John Doe', 
                citeUrl: 'https://www.citeexample.com' 
            }
        } 
    ]

export const portfolioData = 
    [
        {
            id: 1,
            type: 'code',
            card: card1,
            title: 'Some thing',
            text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo',
            url: 'somesite.com'
        },
        {
            id: 2,
            type: 'ui',
            card: card2,
            title: 'Other thing',
            text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo',
            url: 'somesite.com'
        },
        {
            id: 3,
            type: 'code',
            card: card1,
            title: 'Some thing',
            text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo',
            url: 'somesite.com'
        },
        {
            id: 4,
            type: 'ui',
            card: card2,
            title: 'Other thing',
            text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo',
            url: 'somesite.com'
        },
    ]

    