import Box from "../components/Box/Box"
import Panel from "../components/Panel/Panel"
import Info from "../components/Info/Info"
import './Inner.scss'
import Timeline from "../components/Timeline/Timeline"
import { HashLink } from "react-router-hash-link"
import { useState } from "react"
import Portfolio from "../components/Portfolio/Portfolio"
import Experience from "../components/Experience/Experience"
import Contacts from "../components/Contacts/Contacts"
import Skills from "../components/Skills/Skills"
import Feedbacks from "../components/Feedbacks/Feedbacks"
import {  experience, feedbacks, portfolioData } from "../mockData"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faChevronUp } from "@fortawesome/free-solid-svg-icons"

const Inner = () => {
    const [navCollapse, setNavCollapse] = useState(false)
    return (
        <div className="inner-page" >
            <Panel navCollapse={navCollapse}/>
            
            <div className={`main-content ${navCollapse ? 'closed' : ''}`}>
                <button className={`hamburger ${navCollapse ? 'collapsed' : 'open'}`} onClick={()=>{setNavCollapse(!navCollapse)}}>
                        <span></span>
                        <span></span>
                        <span></span>
                </button>
                <section className="freespace" id='top'></section>
                <Box title='About Me' id='aboutme' content={<Info text='Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque'/>}/>
                <Box title='Education' id='education' content={<Timeline/>}/>
                <Box title='Experience' id='experience' content={<Experience data={experience}/>}/>
                <Box title='Skills' id='skills' content={<Skills/>}/>
                <Box title='Portfolio' id='portfolio' content={<Portfolio portfolioData={portfolioData}/>}/>
                <Box title='Contacts' id='contacts' content={<Contacts/>}/>
                <Box title='Feedbacks' id='feedbacks' content={<Feedbacks data={feedbacks}/>}/>
                <HashLink to='#top' smooth>
                    <button className="bottom">
                        <FontAwesomeIcon icon={faChevronUp}/>
                    </button> 
                </HashLink>
            </div>
        </div>
    )
}

export default Inner