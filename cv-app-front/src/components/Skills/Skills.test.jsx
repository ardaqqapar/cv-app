import React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import { store } from '../../app/store';
import '@testing-library/jest-dom/extend-expect';

import Skills from './Skills';
import skillsReducer from './skillsSlice'

// Mock the necessary Redux store and actions




test('renders the Skills component', () => {
  render(
    <Provider store={store}>
      <Skills />
    </Provider>
  );

  // Add your assertions here
});

test('toggles form visibility on button click', () => {
  render(
    <Provider store={store}>
      <Skills />
    </Provider>
  );

  const openEditButton = screen.getByText('Open edit');
  fireEvent.click(openEditButton);

  expect(screen.getByText('Close edit')).toBeInTheDocument();

  fireEvent.click(openEditButton);

  expect(screen.getByText('Open edit')).toBeInTheDocument();
});

// test('submits skill form and adds a new skill', async () => {
//   render(
//     <Provider store={store}>
//       <Skills />
//     </Provider>
//   );

//   const openEditButton = screen.getByText('Open edit');
//   fireEvent.click(openEditButton);

//   const skillNameInput = screen.getByPlaceholderText('Enter skill name');
//   const skillRangeInput = screen.getByPlaceholderText('Enter skill range');
//   const addSkillButton = screen.getByRole('button', {name: ''})

//   fireEvent.change(skillNameInput, { target: { value: 'Test Skill' } });
//   fireEvent.change(skillRangeInput, { target: { value: '50' } });

//   fireEvent.click(addSkillButton);

//   await waitFor(() => {
//     expect(screen.getByText('Test Skill')).toBeInTheDocument();
//   });
// });
