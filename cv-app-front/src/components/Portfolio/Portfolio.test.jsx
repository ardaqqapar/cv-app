import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Portfolio from './Portfolio';
import '@testing-library/jest-dom/extend-expect';

describe('Portfolio', () => {
  const mockData = [
    {
      id: 1,
      title: 'Project 1',
      text: 'Lorem ipsum dolor sit amet',
      url: 'https://example.com',
      type: 'code',
      card: 'path/to/card1.jpg',
    },
    {
      id: 2,
      title: 'Project 2',
      text: 'Lorem ipsum dolor sit amet',
      url: 'https://example.com',
      type: 'ui',
      card: 'path/to/card2.jpg',
    },
  ];

  test('renders portfolio items with correct data', () => {
    const { getByTestId, getByText, queryByText } = render(
      <Portfolio portfolioData={mockData} />
    );

    const portfolio = getByTestId('portfolio');
    const filterAll = getByText('All');
    const filterCode = getByText('Code');
    const filterUI = getByText('UI');

    expect(portfolio).toBeInTheDocument();
    expect(filterAll).toBeInTheDocument();
    expect(filterCode).toBeInTheDocument();
    expect(filterUI).toBeInTheDocument();

    const project1 = getByText('Project 1');
    const project2 = getByText('Project 2');
    expect(project1).toBeInTheDocument();
    expect(project2).toBeInTheDocument();
    expect(queryByText('Nonexistent Project')).toBeNull();
  });

  test('applies correct filter when filter tags are clicked', () => {
    const { getByTestId, getByText, queryByText } = render(
      <Portfolio portfolioData={mockData} />
    );

    const filterAll = getByText('All');
    const filterCode = getByText('Code');
    const filterUI = getByText('UI');

    fireEvent.click(filterCode);
    expect(getByTestId('portfolio-list').children.length).toBe(1);
    expect(queryByText('Project 2')).toBeNull();

    fireEvent.click(filterUI);
    expect(getByTestId('portfolio-list').children.length).toBe(1);
    expect(queryByText('Project 1')).toBeNull();

    fireEvent.click(filterAll);
    expect(getByTestId('portfolio-list').children.length).toBe(2);
  });

  // Add more tests if needed
});
