import guy from './assets/guy.png'
import card1 from './assets/Portfolio/card_1.png'
import card2 from './assets/Portfolio/card_2.png'

export const education = 
    [ 
        {
            "date": 2001, 
            "title": "Title 0", 
            "text": "Elit voluptate ad nostrud laboris. Elit incididunt mollit enim enim id id laboris dolore et et mollit. Mollit adipisicing ullamco exercitation ullamco proident aute enim nisi. Dolore eu fugiat consectetur nulla sunt Lorem ex ad. Anim eiusmod do tempor fugiat minim do aliqua amet ex dolore velit.\r\n" 
        }, 
        { 
            "date": 2000, 
            "title": "Title 1", 
            "text": "Et irure culpa ad proident labore excepteur elit dolore. Quis commodo elit culpa eiusmod dolor proident non commodo excepteur aute duis duis eu fugiat. Eu duis occaecat nulla eiusmod non esse cillum est aute elit amet cillum commodo.\r\n" 
        }, 
        { 
            "date": 2012, 
            "title": "Title 2", 
            "text": "Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrud. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod aliquip voluptate. Velit magna labore eiusmod eiusmod labore amet eiusmod. In duis eiusmod commodo duis. Exercitation Lorem sint do aliquip veniam duis elit quis culpa irure quis nulla. Reprehenderit fugiat amet sint commodo ex.\r\n" 
        },
        {
            "date": 2003, 
            "title": "Title 0", 
            "text": "Elit voluptate ad nostrud laboris. Elit incididunt mollit enim enim id id laboris dolore et et mollit. Mollit adipisicing ullamco exercitation ullamco proident aute enim nisi. Dolore eu fugiat consectetur nulla sunt Lorem ex ad. Anim eiusmod do tempor fugiat minim do aliqua amet ex dolore velit.\r\n" 
        }, 
        { 
            "date": 2004, 
            "title": "Title 1", 
            "text": "Et irure culpa ad proident labore excepteur elit dolore. Quis commodo elit culpa eiusmod dolor proident non commodo excepteur aute duis duis eu fugiat. Eu duis occaecat nulla eiusmod non esse cillum est aute elit amet cillum commodo.\r\n" 
        }, 
        // { 
        //     "date": 2005, 
        //     "title": "Title 2", 
        //     "text": "Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrud. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod aliquip voluptate. Velit magna labore eiusmod eiusmod labore amet eiusmod. In duis eiusmod commodo duis. Exercitation Lorem sint do aliquip veniam duis elit quis culpa irure quis nulla. Reprehenderit fugiat amet sint commodo ex.\r\n" 
        // }  
    ]

export const experience = 
    [
        {
            id: 1,
            date: '2013',
            company: 'Google',
            job: 'Front-end developer / php programmer',
            description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor'
        },
        {
            id: 2,
            date: '2012', 
            company: 'Twitter',
            job: 'Web developer',
            description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor'
        }
    ]

export const feedbacks = 
    [ 
        {
            id: 1,
            feedback: ' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor', 
            reporter: { 
                photoUrl: guy, 
                name: 'John Doe', 
                citeUrl: 'https://www.citeexample.com' 
            } 
        }, 
        {
            id: 2,
            feedback: ' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor', 
            reporter: { 
                photoUrl: guy, 
                name: 'John Doe', 
                citeUrl: 'https://www.citeexample.com' 
            }
        } 
    ]

export const portfolioData = 
    [
        {
            id: 1,
            type: 'code',
            card: card1,
            title: 'Some thing',
            text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo',
            url: 'somesite.com'
        },
        {
            id: 2,
            type: 'ui',
            card: card2,
            title: 'Other thing',
            text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo',
            url: 'somesite.com'
        },
        {
            id: 3,
            type: 'code',
            card: card1,
            title: 'Some thing',
            text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo',
            url: 'somesite.com'
        },
        {
            id: 4,
            type: 'ui',
            card: card2,
            title: 'Other thing',
            text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo',
            url: 'somesite.com'
        },
    ]

    