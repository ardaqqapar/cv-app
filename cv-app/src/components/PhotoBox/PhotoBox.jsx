import avatar from '../../assets/avatar.png'
import './PhotoBox.scss'
const PhotoBox = ( {name, title, description, type } ) => {
    return (
        <div className={`photoBox ${type}`}>
            <img src={avatar}/>
            <h1>{name}</h1>
            <h2>{title}</h2>
            <p>{description}</p>
        </div>
    )
}

export default PhotoBox