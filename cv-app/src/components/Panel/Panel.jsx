import PhotoBox from "../PhotoBox/PhotoBox"
import Button from "../Button/Button"
import './Panel.scss'
import Navigation from "../Navigation/Navigation"
import { useState } from "react"
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons"
const Panel = ({navCollapse}) => {
    
    
    return (
        <div className={`panel ${navCollapse ? "navCollapse" : ""}`}>
            <PhotoBox className='panelPhotoBox' type='small' name='Ardak Kapar'/>
            <Navigation/>
            <div className="buttonSpace">
                <Button text='Go back' link='/' icon={faChevronLeft}/>
            </div>
        </div>
    )
}

export default Panel