import './Skills.scss'

const Skills = () => {
    return (
      <div className="skills-container">
        <div className="skill">
          <div className="skill-progress">
            <div className="skill-name">JavaScript</div>
            <div className="progress" style={{  width: '90%'}}></div>
          </div>
        </div>
        <div className="skill">
          <div className="skill-progress">
            <div className="skill-name">React</div>
            <div className="progress" style={{ width: '75%'}}></div>
          </div>
        </div>
        <div className="breakpoints">
          <div className="breakpoint"></div>
          <div className="breakpoint"></div>
          <div className="breakpoint"></div>
          <div className="breakpoint"></div>
        </div>
        <div className="skill-levels">
            <p>Beginner</p>
            <p>Proficient</p>
            <p>Expert</p>
            <p>Master</p>
        </div>
      </div>
    );
  }

export default Skills