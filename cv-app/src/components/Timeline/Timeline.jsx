import './Timeline.scss'
const Timeline = ( {data} ) => {
    return (

            <div className="timeline">
                {data.map((job)=>(
                    <div className="container" key={job.date}>
                        <div className="date">{job.date}</div>
                        <div className="text-box">
                            <h2>{job.title}</h2>
                            <p>{job.text}</p>
                            <span className='pointer-arrow'></span>
                        </div>
                    </div>
                ))}
            </div>
    )
}

export default Timeline