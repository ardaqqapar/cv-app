import './Info.scss'
const Info = ({text}) => {
    return(
        <p className='info-box'>
            {text}
        </p>
    )
} 

export default Info