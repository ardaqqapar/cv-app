import './Box.scss'

const Box = ( {title, id, content} ) => {
    return(
        <div className='box' id={id}>
            <h1>{title}</h1>
            <div>{content}</div>
        </div>
    )
}

export default Box