import './Experience.scss'

const Experience = ({data}) => {
    return(
        <div className='experience'>
            {data.map((job)=> (
                <div className="wrapper" key={job.id}>
                    <div className="company">{job.company}</div>
                    
                    <div className="position">{job.job}</div>
                    <div className="date-of-work">{job.date}</div>
                    <div className="description">{job.description}</div>
                </div>
            ))}
        </div>
    )
}

export default Experience