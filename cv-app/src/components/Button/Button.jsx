import { Link } from "react-router-dom"
import './Button.scss'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

const Button = ( {icon, text, link} ) => {
    return (
        <Link to={link}>
            <button className={icon ? 'button small' : 'button'}>
                <FontAwesomeIcon icon={icon}/>
                <p>{text}</p>
            </button>
        </Link>
    )
}
export default Button