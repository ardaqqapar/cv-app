import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.scss'
import Inner from './pages/Inner.jsx'
import { BrowserRouter, Routes, Route } from 'react-router-dom'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <BrowserRouter>
        <Routes>
          <Route path='/' element={<App />}/>
          <Route path='/inner' element={<Inner/>}/>
          {/* <Route path='/community/:userId' element={<User/>}/>  
          <Route path="*" element={<NotFound />} /> */}
        </Routes>
      </BrowserRouter>
  </React.StrictMode>,
)
